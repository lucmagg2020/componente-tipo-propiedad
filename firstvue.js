let MyProps = Vue.component('my-props', {
    template: `<div component="my-props">
                    <p>Texto requerido:{{ myRequired }}*</p>
                    <p>Objeto:{{ CompleteName }}</p>
                    <p<Multiple Valor:{{ myMultipleValue }}</p>
                    Array:<ul v-for="item in myArray">
                            <li>{{ item }}</li>
                          </ul>
                </div>`,
    data(){
        return{
            myRequired: this.propRequired,
            myArray: this.propArray,
            myMultipleValue: this.propMultipleValue
        }
    },
    computed:{
        CompleteName(){
            if(this.propObject && this.propObject.Name && this.propObject.LastName)
                return this.propObject.Name+' '+this.propObject.LastName
            else
                return 'falta nombre'
        }
    },
    props:{
        propRequired: {type:String, required:true},
        propArray: {type:Array, default:()=>{return[]}},
        propObject: {type:Object, default:()=>{return {}}},
        propMultipleValue: [Number,String,Date]
    }

})